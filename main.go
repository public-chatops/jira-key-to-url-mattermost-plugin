package main

import (
	"fmt"

	"github.com/mattermost/mattermost-server/plugin"

	"github.com/sirupsen/logrus"
)

var (
	Application = "Jira Key to URL Mattermost Plugin"

	GitSha1        = "dev" //injected at build time
	BuildFlavor    = "dev" //injected at build time
	DisplayVersion = "dev" //injected at build time
)

func main() {
	logrus.Println(fmt.Sprintf("DisplayVersion=%s, GitSha1=%s, BuildFlavor=%s", DisplayVersion, GitSha1, BuildFlavor))
	plugin.ClientMain(&Plugin{})
}
