package main

import (
	"fmt"
	"regexp"
	"strings"
)

var issueKeyPattern = regexp.MustCompile(`([a-zA-Z0-9]+)-[0-9]+`)

func replaceProjectKeysWithURLs(text string, allowedProjKeys []string, urlFormat string) string {
	// issueKeyPattern.ReplaceAllString(src, repl)
	allMatchIndexes := issueKeyPattern.FindAllStringSubmatchIndex(text, -1)
	if len(allMatchIndexes) == 0 {
		return text
	}

	newText := ""
	for i, matchIndexes := range allMatchIndexes {
		if i == 0 {
			newText += text[0:matchIndexes[0]]
		} else {
			newText += text[allMatchIndexes[i-1][1]:matchIndexes[0]]
		}

		projStartIndex := matchIndexes[2]
		projEndIndex := matchIndexes[3] //index immediately after end
		projKey := text[projStartIndex:projEndIndex]

		issueStartIndex := matchIndexes[0]
		issueEndIndex := matchIndexes[1]
		issueKey := text[issueStartIndex:issueEndIndex]

		if !projKeyIsAllowed(allowedProjKeys, projKey) {
			newText += issueKey
			continue
		}

		expandedURL := strings.Replace(strings.TrimSpace(urlFormat), keyReplacePlaceholder, issueKey, -1)
		if strings.Contains(text, expandedURL) {
			//for the special case where the key appears in the text but its url/link is also already there
			newText += issueKey
			continue
		}
		markdownExpandedURL := fmt.Sprintf("[%s](%s)", issueKey, expandedURL)
		newText += markdownExpandedURL
	}

	newText += text[allMatchIndexes[len(allMatchIndexes)-1][1]:]

	return newText
}

func projKeyIsAllowed(allowedProjKeys []string, key string) bool {
	for _, k := range allowedProjKeys {
		if strings.EqualFold(k, key) {
			return true
		}
	}
	return false
}
