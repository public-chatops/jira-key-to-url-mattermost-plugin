package main

import (
	"fmt"
	"strings"
)

const (
	keyReplacePlaceholder = "{{ISSUE_KEY}}"
)

type Configuration struct {
	SentryDSN   string
	ProjectKeys string
	URLFormat   string
}

func (c *Configuration) IsValid() error {
	if strings.TrimSpace(c.SentryDSN) == "" {
		return fmt.Errorf("SentryDSN is not configured.")
	}

	if strings.TrimSpace(c.ProjectKeys) == "" {
		return fmt.Errorf("ProjectKeys is not configured.")
	}
	if len(c.SplitProjectKeys()) == 0 {
		return fmt.Errorf("ProjectKeys requires at least one non empty key (comma seperated)")
	}

	if strings.TrimSpace(c.URLFormat) == "" {
		return fmt.Errorf("URLFormat is not configured.")
	}
	if !strings.Contains(c.URLFormat, keyReplacePlaceholder) {
		return fmt.Errorf("URLFormat must contain placeholder " + keyReplacePlaceholder)
	}

	return nil
}

func (c *Configuration) SplitProjectKeys() []string {
	split := strings.Split(c.ProjectKeys, ",")

	keys := []string{}
	for _, x := range split {
		trimmed := strings.TrimSpace(x)
		if trimmed == "" {
			continue
		}
		keys = append(keys, trimmed)
	}
	return keys
}
