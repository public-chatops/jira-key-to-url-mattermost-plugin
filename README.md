# jira-key-to-url-mattermost-plugin

## Installing the plugin

* Either:
  * Download the gitlab built artifact to get the `jira-key-to-url.tar.gz` file, or
  * Package it manually by calling `package.bat`
* Browse to the Mattesmost admin system console and upload plugin (do not activate yet)
* Configure `Jira Key to URL` in the Plugins section of system console
* SSH into container and run `chmod +x /mattermost/plugins/jira-key-to-url/plugin.bin`
* Activate plugin