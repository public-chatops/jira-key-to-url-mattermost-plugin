package main

import "testing"

func Test_replaceProjectKeysWithURLs(t *testing.T) {
	type args struct {
		text      string
		keys      []string
		urlFormat string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"starts with key",
			args{
				"PROJ1-456 is my next focus",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"[PROJ1-456](https://example-jira.com/browse/PROJ1-456) is my next focus",
		},

		{
			"ends with key",
			args{
				"Working on PROJ1-774",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"Working on [PROJ1-774](https://example-jira.com/browse/PROJ1-774)",
		},

		{
			"start and end with key",
			args{
				"PROJ1-222 is first and then focusing on PROJ1-333",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"[PROJ1-222](https://example-jira.com/browse/PROJ1-222) is first and then focusing on [PROJ1-333](https://example-jira.com/browse/PROJ1-333)",
		},

		{
			"issue in middle",
			args{
				"Working on PROJ1-774 in the text",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"Working on [PROJ1-774](https://example-jira.com/browse/PROJ1-774) in the text",
		},

		{
			"multiple issues in middle",
			args{
				"Working on both PROJ1-774, PROJ1-789 and PROJ1-4 in the text",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"Working on both [PROJ1-774](https://example-jira.com/browse/PROJ1-774), [PROJ1-789](https://example-jira.com/browse/PROJ1-789) and [PROJ1-4](https://example-jira.com/browse/PROJ1-4) in the text",
		},

		{
			"only issue",
			args{
				"PR-774",
				[]string{"PR"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"[PR-774](https://example-jira.com/browse/PR-774)",
		},

		{
			"empty msg",
			args{
				"",
				[]string{"PR"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"",
		},

		{
			"no issue in msg",
			args{
				"This is a message",
				[]string{"PR"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"This is a message",
		},

		{
			"key not allowed",
			args{
				"PROJ1-774",
				[]string{"PROJ2"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"PROJ1-774",
		},

		{
			"multiple keys, only one allowed",
			args{
				"PROJ1-111 and PROJ2-222 is first then going to PROJ1-111",
				[]string{"PROJ2"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"PROJ1-111 and [PROJ2-222](https://example-jira.com/browse/PROJ2-222) is first then going to PROJ1-111",
		},

		{
			"multiple keys, only one not allowed",
			args{
				"PROJ1-111 and PROJ2-222 is first then going to PROJ1-111",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"[PROJ1-111](https://example-jira.com/browse/PROJ1-111) and PROJ2-222 is first then going to [PROJ1-111](https://example-jira.com/browse/PROJ1-111)",
		},

		{
			"already contains a markdown link",
			args{
				"[PROJ1-111](https://example-jira.com/browse/PROJ1-111)",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"[PROJ1-111](https://example-jira.com/browse/PROJ1-111)",
		},

		{
			"already contains a markdown link 2",
			args{
				"Check out https://example-jira.com/browse/PROJ1-26566?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"Check out https://example-jira.com/browse/PROJ1-26566?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel",
		},

		{
			"Format contains spaces 1",
			args{
				"Check out https://example-jira.com/browse/PROJ1-26566?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}} ",
			},
			"Check out https://example-jira.com/browse/PROJ1-26566?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel",
		},
		{
			"Format contains spaces 2",
			args{
				"Check out https://example-jira.com/browse/PROJ1-26566?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel",
				[]string{"PROJ1"}, " https://example-jira.com/browse/{{ISSUE_KEY}} ",
			},
			"Check out https://example-jira.com/browse/PROJ1-26566?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel",
		},

		{
			"already contains a non-markdown link",
			args{
				"https://example-jira.com/browse/PROJ1-111",
				[]string{"PROJ1"}, "https://example-jira.com/browse/{{ISSUE_KEY}}",
			},
			"https://example-jira.com/browse/PROJ1-111",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := replaceProjectKeysWithURLs(tt.args.text, tt.args.keys, tt.args.urlFormat); got != tt.want {
				t.Errorf("replaceProjectKeysWithURLs() = `%v`, want `%v`", got, tt.want)
			}
		})
	}
}
