package main

import (
	"sync/atomic"

	"github.com/mattermost/mattermost-server/model"
	"github.com/mattermost/mattermost-server/plugin"

	"github.com/SierraSoftworks/sentry-go"
	"github.com/sirupsen/logrus"
)

type Plugin struct {
	plugin.MattermostPlugin
	configuration atomic.Value
	sentryClient  sentry.Client
}

func (p *Plugin) config() *Configuration { return p.configuration.Load().(*Configuration) }

func (p *Plugin) reportOrLogError(err error, message string) {
	if p.sentryClient != nil {
		p.sentryClient.Capture(sentry.ExceptionForError(err), sentry.Message("Plugin configuration is invalid")).Wait()
	} else {
		logrus.WithError(err).Error(message)
	}
}

func (p *Plugin) OnActivate() error {
	if err := p.OnConfigurationChange(); err != nil {
		return err
	}

	config := p.config()
	if err := config.IsValid(); err != nil {
		return err
	}

	p.sentryClient = sentry.NewClient(sentry.DSN(config.SentryDSN), sentry.Release(DisplayVersion))

	sentry.DefaultBreadcrumbs().NewDefault(nil).WithMessage(Application + " activated").WithCategory("log")
	p.sentryClient.Capture(sentry.Message(Application+" activated"), sentry.Level(sentry.Debug)).Wait()

	return nil
}

func (p *Plugin) OnDeactivate() error {
	if p.sentryClient != nil {
		p.sentryClient.Capture(sentry.Message(Application+" Finished"), sentry.Level(sentry.Debug)).Wait()
	}
	return nil
}

func (p *Plugin) OnConfigurationChange() error {
	var configuration Configuration
	err := p.API.LoadPluginConfiguration(&configuration)
	p.configuration.Store(&configuration)
	return err
}

func (p *Plugin) MessageWillBePosted(c *plugin.Context, post *model.Post) (*model.Post, string) {
	rejectReason := ""

	config := p.config()

	if err := config.IsValid(); err != nil {
		p.reportOrLogError(err, "Plugin configuration is invalid")
		return post, ""
	}

	post.Message = replaceProjectKeysWithURLs(post.Message, config.SplitProjectKeys(), config.URLFormat)

	return post, rejectReason
}
